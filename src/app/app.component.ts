import { Component } from '@angular/core';
import { ThemeService } from './core/theme.service';
import { TimerService } from './core/timer.service';

@Component({
  selector: 'app-root',
  template: `
    <div>RXJS Demo</div>
    <button routerLink="demo1" #btn>demo1</button>
    <button routerLink="demo2">demo2</button>
    <button routerLink="demo3">demo3</button>
    <button routerLink="demo4">demo4</button>
    <button routerLink="demo5">demo5</button>
    <button routerLink="demo6/6">demo6</button>
    <button routerLink="demo7">demo7</button>
    <button routerLink="demo8">demo8</button>
    <button routerLink="demo9">demo9</button>
    <button routerLink="demo10">demo10</button>
    <button routerLink="demo11">demo11</button>
    
    <button (click)="themeService.changeTheme('light')">light</button>
    <button (click)="themeService.changeTheme('dark')">dark</button>
    {{timerService.timer$ | async}}
    <hr>
    
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {

  constructor(
    public themeService: ThemeService,
    public timerService: TimerService
  ) {
  }
}
