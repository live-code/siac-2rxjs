import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  catchError,
  concatMap,
  debounceTime,
  delay,
  fromEvent,
  interval,
  map,
  mergeAll,
  mergeMap, of,
  Subscription,
  switchMap,
  tap
} from 'rxjs';
import { User } from '../../model/user';

@Component({
  selector: 'app-demo3',
  template: `
    <input type="text" [formControl]="myReactiveInput">
    
    <li *ngFor="let user of users$ | async">{{user.name}}</li>
    
  `,
})
export class Demo3Component  {
  myReactiveInput = new FormControl('');

  users$ =  this.myReactiveInput.valueChanges
    .pipe(
      debounceTime(1000),
      mergeMap(
        (text) => this.http.get<User[]>('https://jsonplaceholder.typicode.com/users?q=' + text)
          .pipe(
            delay(2000),
            catchError(() => {
              return of([])
            })
          )
      ),
    )


  constructor(private http: HttpClient) {
  }

}
