import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ThemeService } from '../../core/theme.service';

@Component({
  selector: 'app-demo10',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
      demo10 works!
      <div>obs: {{themeService.theme$ | async}}</div>
      {{render()}}
  `,
})
export class Demo10Component {

  constructor(public themeService: ThemeService) {
  }

  render() {
    console.log('render')
  }
}
