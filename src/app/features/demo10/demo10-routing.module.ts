import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Demo10Component } from './demo10.component';

const routes: Routes = [{ path: '', component: Demo10Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Demo10RoutingModule { }
