import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Demo10RoutingModule } from './demo10-routing.module';
import { Demo10Component } from './demo10.component';


@NgModule({
  declarations: [
    Demo10Component
  ],
  imports: [
    CommonModule,
    Demo10RoutingModule
  ]
})
export class Demo10Module { }
