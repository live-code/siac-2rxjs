import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { interval, share, shareReplay, Subscription, take } from 'rxjs';

@Component({
  selector: 'app-demo9',
  template: `
    <h1>Async  {{timer$ | async}}</h1>
    <button (click)="startTimer()">Subscribe Again</button>
    {{req$ | async | json}}
    {{req$ | async | json}}
  `,
})
export class Demo9Component {
  req$ = this.http.get('https://jsonplaceholder.typicode.com/users')
    .pipe(
      shareReplay(1)
    )

  timer$ = interval(1000)
    .pipe(
      shareReplay({ refCount: true })
    )

  sub!: Subscription
  constructor(private http: HttpClient) {
  }

  startTimer() {
    this.req$.subscribe({
      next: (res) => console.log(res),
      error: (err) => console.log('ahia!'),
      complete: () => console.log('completed')
    })


    this.sub = this.timer$.subscribe({
      next: (res) => console.log('B', res),
      error: (err) => console.log('ahia!'),
      complete: () => console.log('completed')
    })

  }

  ngOnDestroy() {
    if (this.sub)
      this.sub.unsubscribe()
  }
}
