import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Demo9RoutingModule } from './demo9-routing.module';
import { Demo9Component } from './demo9.component';


@NgModule({
  declarations: [
    Demo9Component
  ],
  imports: [
    CommonModule,
    Demo9RoutingModule
  ]
})
export class Demo9Module { }
