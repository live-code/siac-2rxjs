import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Demo9Component } from './demo9.component';

const routes: Routes = [{ path: '', component: Demo9Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Demo9RoutingModule { }
