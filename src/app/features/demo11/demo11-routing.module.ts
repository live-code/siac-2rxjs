import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Demo11Component } from './demo11.component';

const routes: Routes = [{ path: '', component: Demo11Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Demo11RoutingModule { }
