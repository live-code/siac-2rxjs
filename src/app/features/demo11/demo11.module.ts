import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Demo11RoutingModule } from './demo11-routing.module';
import { Demo11Component } from './demo11.component';


@NgModule({
  declarations: [
    Demo11Component
  ],
  imports: [
    CommonModule,
    Demo11RoutingModule
  ]
})
export class Demo11Module { }
