import { Component } from '@angular/core';
import { TimerService } from '../../core/timer.service';

@Component({
  selector: 'app-demo11',
  template: `
    <p>
      demo11 works!
      {{timerSrv.timer$ | async}}
    </p>
  `,
  styles: [
  ]
})
export class Demo11Component {

  constructor(public timerSrv: TimerService) {
  }
}
