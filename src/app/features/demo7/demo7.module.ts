import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Demo7RoutingModule } from './demo7-routing.module';
import { Demo7Component } from './demo7.component';


@NgModule({
  declarations: [
    Demo7Component
  ],
  imports: [
    CommonModule,
    Demo7RoutingModule
  ]
})
export class Demo7Module { }
