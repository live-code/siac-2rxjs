import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { concatMap, delay, filter, interval, map, mergeMap, of, switchMap, tap, toArray } from 'rxjs';
import { Todo } from '../../model/todo';
import { User } from '../../model/user';

@Component({
  selector: 'app-demo7',
  template: `
    <h1>MergeMap return array</h1>
    
    <ul>
     <li *ngFor="let obj of data$ | async">
       {{obj.user.name}}
       <ul *ngFor="let todo of obj.todos">
         <li>
           <input type="checkbox" [checked]="todo.completed">
           {{todo.title}}
         </li>
       </ul>
     </li>
    </ul>
    
    <pre>{{data | json}}</pre>
  `,
})
export class Demo7Component {
  data: { user: User, todos: Todo[] }[] = [];

  data$ = this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
    .pipe(
      mergeMap(users => users),
      filter(user => user.id > 5 ),
      concatMap(
        user => this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos?userId=' + user.id)
          .pipe(
            map(todos => ({ user, todos }))
          )
      ),
      toArray()
    )
  constructor(private http: HttpClient) {


  }

}
