import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Demo8RoutingModule } from './demo8-routing.module';
import { Demo8Component } from './demo8.component';


@NgModule({
  declarations: [
    Demo8Component
  ],
  imports: [
    CommonModule,
    Demo8RoutingModule
  ]
})
export class Demo8Module { }
