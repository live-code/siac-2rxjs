import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Demo8Component } from './demo8.component';

const routes: Routes = [{ path: '', component: Demo8Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Demo8RoutingModule { }
