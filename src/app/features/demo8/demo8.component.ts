import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { forkJoin, mergeMap } from 'rxjs';
import { Post } from '../../model/post';
import { Todo } from '../../model/todo';
import { User } from '../../model/user';

@Component({
  selector: 'app-demo8',
  template: `
    <h1>Example nested and multple calls</h1>
    <pre>See console</pre>
  `,
  styles: [
  ]
})
export class Demo8Component {

  constructor(private http: HttpClient) {
    http.get<Post>('https://jsonplaceholder.typicode.com/posts/1')
      .pipe(
        mergeMap(post => {
          return forkJoin({
            todos: this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos?userId=' + post.userId),
            comments: this.http.get<any[]>('https://jsonplaceholder.typicode.com/comments?postId' + post.id),
          })
        })
      )
      .subscribe(data => console.log(data))

    forkJoin({
      todos: this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos'),
      comments: this.http.get<any[]>('https://jsonplaceholder.typicode.com/comments'),
    })
      .pipe(
        mergeMap(data => http.get<Post>('https://jsonplaceholder.typicode.com/posts/' + data.comments[0].id + data.todos[0].id))
      )
      .subscribe(console.log)

  }
}
