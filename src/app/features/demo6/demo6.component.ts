import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, mergeMap, tap } from 'rxjs';
import { Post } from '../../model/post';
import { User } from '../../model/user';

@Component({
  selector: 'app-demo6',
  template: `
    
    <h1>Nested XHR with router params</h1>
    <div>
      <h2>{{(data$ | async)?.user?.name}}</h2>
      <hr>
      <div>{{(data$ | async)?.post?.title}}</div>
      <div>{{(data$ | async)?.post?.body}}</div>
    </div>
    <hr>
    
    Change User:
    <button routerLink="/demo6/27">27</button>
    <button routerLink="/demo6/45">45</button>
    <button routerLink="/demo6/78">78</button>
  `,
  styles: [
  ]
})
export class Demo6Component {
  data: { user: User, post: Post } | undefined;

  // async pipe
  data$ = this.activateRoute.params
    .pipe(
      mergeMap(params => this.http.get<Post>(`https://jsonplaceholder.typicode.com/posts/${params['postId']}`)
        .pipe(

          mergeMap(
            post => this.http.get<User>(`https://jsonplaceholder.typicode.com/users/${post.userId}`)
              .pipe(
                map(user => {
                  return {
                    user,
                    post
                  }
                }),
              )
          ),
        ))
    )

  // manual subscribe
  constructor(private http: HttpClient, private activateRoute: ActivatedRoute) {
    activateRoute.params
      .pipe(
        mergeMap(params => http.get<Post>(`https://jsonplaceholder.typicode.com/posts/${params['postId']}`)
          .pipe(

            mergeMap(
              post => http.get<User>(`https://jsonplaceholder.typicode.com/users/${post.userId}`)
                .pipe(
                  map(user => {
                    return {
                      user,
                      post
                    }
                  }),
                )
            ),
          ))
      )
      .subscribe(data => {
        this.data = data;
      })

  }

}


interface Data {
  user: User;
  post: Post;
}
