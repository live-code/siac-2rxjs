import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { combineLatest, delay, forkJoin, interval, of, Subscription, take } from 'rxjs';
import { Post } from '../../model/post';
import { User } from '../../model/user';

@Component({
  selector: 'app-demo2',
  template: `
    <p>
      demo2 works!
    </p>

    <div *ngIf="(data$ | async) as data">
      <li *ngFor="let user of data.users">{{user.name}}</li>
      <li *ngFor="let user of data.posts">{{user.title}}</li>
    </div>
  `,
})
export class Demo2Component {
  data$ =  forkJoin({
    users: this.http.get<User[]>('https://jsonplaceholder.typicode.com/users'),
    posts: this.http.get<Post[]>('https://jsonplaceholder.typicode.com/posts'),
  })

  constructor(private http: HttpClient) {
  }
}
