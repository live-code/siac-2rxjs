import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { map, mergeMap, tap } from 'rxjs';
import { Post } from '../../model/post';
import { User } from '../../model/user';

@Component({
  selector: 'app-demo5',
  template: `
    
    <h1>Nested XHR</h1>
    <div>
      <h2>{{data?.user?.name}}</h2>
      <hr>
      <div>{{data?.post?.title}}</div>
      <div>{{data?.post?.body}}</div>
    </div>
  `,
  styles: [
  ]
})
export class Demo5Component {
  data: { user: User, post: Post } | undefined;

  constructor(http: HttpClient) {
    http.get<Post>('https://jsonplaceholder.typicode.com/posts/14')
      .pipe(

        mergeMap(
          post => http.get<User>(`https://jsonplaceholder.typicode.com/users/${post.userId}`)
            .pipe(
              map(user => {
                return {
                  user,
                  post
                }
              }),
              // versione compatta
              // map(user => ({ user, post }))
            )
        ),

      )
      .subscribe((data) => {
        this.data = data;
      });
  }

}


interface Data {
  user: User;
  post: Post;
}
