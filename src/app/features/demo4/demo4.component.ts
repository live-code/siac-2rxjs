import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { catchError, debounceTime, distinct, distinctUntilChanged, filter, mergeMap, of, retry } from 'rxjs';
import { MyServiceService } from '../../core/my-service.service';
import { Meteo } from '../../model/meteo';

@Component({
  selector: 'app-demo4',
  template: `
    <p>
      demo4 works!
    </p>

    <input type="text" [formControl]="input">
    
    <pre>{{meteo | json}}</pre>
  `,
})
export class Demo4Component {
  input = new FormControl('', { nonNullable: true });
  meteo: Meteo | null = null;

  constructor(private http: HttpClient) {
    this.input.valueChanges
      .pipe(
        filter(text => text.length > 3),
        debounceTime(1000),
        distinctUntilChanged(),
        mergeMap(
          text => this.http.get<Meteo>(`http://api.openweathermap.org/data/2.5/weather?q=${text}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
            .pipe(
              // retry({ count: 2, delay: 1000}),
              catchError(err => {
                return of(null)
              })
            )
        ),

      )
      .subscribe({
        next: (meteo) => {
            this.meteo  = meteo;
        },
        error: (err) => {
          console.log('errore', err)
        }
      })




  }
}
