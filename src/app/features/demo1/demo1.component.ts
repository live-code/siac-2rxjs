import { Component } from '@angular/core';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-demo1',
  template: `
    {{1 | user | async}}
    {{4 | user | async}}
    {{routeID | user | async}}
  `,
})
export class Demo1Component {
  routeID = 5

}
