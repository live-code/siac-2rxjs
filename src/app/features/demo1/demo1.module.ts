import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

import { Demo1RoutingModule } from './demo1-routing.module';
import { Demo1Component } from './demo1.component';


@NgModule({
  declarations: [
    Demo1Component
  ],
  imports: [
    CommonModule,
    Demo1RoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class Demo1Module { }
