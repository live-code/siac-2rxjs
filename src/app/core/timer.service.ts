import { Injectable } from '@angular/core';
import { BehaviorSubject, interval, share } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimerService {
  private _timer$ = interval(1000)
  public timer$ = new BehaviorSubject(0)

  constructor() {
    this._timer$.subscribe(this.timer$)
  }
}

