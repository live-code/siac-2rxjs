import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay, Observable, retry } from 'rxjs';
import { MyServiceService } from './my-service.service';

@Injectable()
export class RetryInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      return next.handle(req)
        .pipe(
          //delay(environment.production ? 0 : 1000)
          retry({ count: 1, delay: 1000})
        )
  }

}
