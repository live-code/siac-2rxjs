import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  theme$ = new BehaviorSubject('dark');
  theme = 'dark';

  constructor() {
    const theme = localStorage.getItem('theme')
    if (theme) {
      this.theme$.next(theme)
    }
  }

  changeTheme(newTheme: string) {
    localStorage.setItem('theme', newTheme)
    this.theme$.next(newTheme)
    this.theme = newTheme
  }
}
