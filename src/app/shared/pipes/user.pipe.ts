import { HttpClient } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { map, Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { User } from '../../model/user';

@Pipe({
  name: 'user'
})
export class UserPipe implements PipeTransform {
  transform(userId: number): Observable<string> {
    return this.http.get<User>(`${environment.BASE_URL}/users/${userId}`)
      .pipe(
        map(res => res.name)
      )
  }

  constructor(private http: HttpClient) {
  }

}
