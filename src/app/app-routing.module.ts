import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'demo1', loadChildren: () => import('./features/demo1/demo1.module').then(m => m.Demo1Module) },
  { path: 'demo2', loadChildren: () => import('./features/demo2/demo2.module').then(m => m.Demo2Module) },
  { path: 'demo3', loadChildren: () => import('./features/demo3/demo3.module').then(m => m.Demo3Module) },
  { path: 'demo4', loadChildren: () => import('./features/demo4/demo4.module').then(m => m.Demo4Module) },
  { path: 'demo5', loadChildren: () => import('./features/demo5/demo5.module').then(m => m.Demo5Module) },
  { path: '', redirectTo: 'demo1', pathMatch: 'full'},
  { path: 'demo6/:postId', loadChildren: () => import('./features/demo6/demo6.module').then(m => m.Demo6Module) },
  { path: 'demo7', loadChildren: () => import('./features/demo7/demo7.module').then(m => m.Demo7Module) },
  { path: 'demo8', loadChildren: () => import('./features/demo8/demo8.module').then(m => m.Demo8Module) },
  { path: 'demo9', loadChildren: () => import('./features/demo9/demo9.module').then(m => m.Demo9Module) },
  { path: 'demo10', loadChildren: () => import('./features/demo10/demo10.module').then(m => m.Demo10Module) },
  { path: 'demo11', loadChildren: () => import('./features/demo11/demo11.module').then(m => m.Demo11Module) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
